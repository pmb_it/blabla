# Description
blabla - an in-game chat to discord gateway for Mortal Online 2.

# Disclaimer
This software was developed as PoC.
No technical support is provided.
If you require more features, you are welcome to fork the project, as it is distributed under the BSD license.

# How it works
You write chat processing rules in natural language in a text file.

For example:
```allow discord messages only when You is the sender. Add @here to the original message text```

The tool monitors for messages of a specified color. When it detects a new message, it employs Optical Character Recognition (OCR) to read the text. 

If the OCR process is successful, the message, along with its base ruleset and the sender's information, is forwarded to ChatGPT. ChatGPT evaluates the message based on predefined rules (see above). 

If the message meets these criteria, it undergoes additional processing before being relayed to a Discord channel.

See manual for details: https://docs.google.com/document/d/1y8Zky76lul6RuvKF7ztLTbXNYwdzcKJIjBD-GBvIP3o/edit?usp=sharing
# License
Copyright 2024 pmb

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

- Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
- Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.