import cv2
import pytesseract
pytesseract.pytesseract.tesseract_cmd = r'c:\Program Files\Tesseract-OCR\tesseract.exe'

import numpy as np
import os
import time
import threading
import dxcam
import re
import configparser
import subprocess
import signal
import tkinter as tk
from configobj import ConfigObj
import keyboard
from openai import OpenAI
from PIL import Image
import pydirectinput
from discordwebhook import Discord
import sys

# globals 
gOpts = {}
gMsg = []
gFrame = None
gDiscord = None

def log(msg):
    '''
    Write log
    '''
    current_time = time.time()
    form_time = time.strftime('%Y-%m-%d-%H:%M:%S', time.localtime(current_time))
    max_log_size = 10 * 1024 * 1024  # 10 MB
    log_name = 'blabla.log'

    with open(log_name, 'a+') as f:
        if os.path.getsize(log_name) > max_log_size:
            f.truncate(0)

        f.write(str(form_time) + ': ' + str(msg) + '\n')
    
    print (str(form_time) + ': ' + str(msg))

def doLineBreak():
    '''
    Make in-game chat line break
    '''
    pydirectinput.press('enter')
    pydirectinput.press('/')
    pydirectinput.press('x')
    pydirectinput.press('enter')

def bgAntiAFK():
    '''
    Character movement (anti-AFK)
    '''
    while True:
        pydirectinput.keyDown('w')
        time.sleep(0.5)
        pydirectinput.keyUp('w')
        time.sleep(300)
        pydirectinput.keyDown('s')
        time.sleep(0.5)
        pydirectinput.keyUp('s')
        
def ocrLocal(fname='ocr.png'):
    '''
    OCR text with pytesseract and process it
    '''
    global gMsg

        
    log('starting ocr')
    parsed_text = pytesseract.image_to_string(Image.open(fname))

    #doLineBreak()
    if len(parsed_text) >=1:
        pattern = r".Guild..(\w+):\s*(.*)" #guild....
        match = re.search(pattern, parsed_text)
        if match:
            doLineBreak()
            sender = match.group(1)  # The second word
            text = match.group(2)  # The remaining part of the string
            #log(len(gMsg))
            #if len(gMsg) > 1:
               # if gMsg[-1][1] == sender and gMsg[-1][2] == text:
              #      log(f'duplicate message {gMsg[-1][1]} {gMsg[-1][2]} {sender} {text}')
             #       return None
            #else:
            log(f'ocr results: sender={sender}, msg={text}')
            gMsg.append((time.time(),sender,text))
            return True
        else:
            log('ocr error')
            return False
        


def startBG(function,th_name=None,**kwargs):
    '''
    Run thread in bg
    '''
    if th_name is not None:
        thread = threading.Thread(target=function,name=th_name,daemon=True,**kwargs)
    else:
        thread = threading.Thread(target=function,daemon=True,**kwargs)
    thread.start()
    log(f'{str(function)} started')
    return thread


def bgCam():
    '''
    Videofeed
    '''
    global gFrame
    cam = dxcam.create(output_color="BGR")
    x,y,w,h = gOpts['chatline']
    bX,bY = x+w,y+h
    region = (x,y,bX,bY)
    while True:
        gFrame = cam.grab(region=region)

        if gFrame is not None:
            anProc(gFrame)
        else:
            continue

def stop_bot():
    '''
    Stop bot
    '''
    log('stopping bot')
    os.kill(os.getpid(), signal.SIGINT)

# 
def reload_bot():
    '''
    Reload bot
    '''
    log('reloading bot')
    starting_command = " ".join(sys.argv) 
    if re.search("blabla.py",starting_command):
        subprocess.Popen(['python','blabla.py']) #fix?
    else:
        subprocess.Popen(starting_command)
    
    stop_bot()

def readConfig():
    '''
    Read config
    '''
    global gOpts
    global gDiscord
    log('loading config file')
    config = configparser.ConfigParser(allow_no_value=True)
    try:
        with open('config.ini') as f:
            config.read_file(f)
    except FileNotFoundError:
        log('config file not found. Did you rename config.ini.templ to config.ini?')
        stop_bot()

    
    _chatline = config.get('XYWH', 'chatline',fallback='0,0,0,0')
    gOpts['chatline'] = list(int(x) for x in _chatline.split(","))
    gOpts['gpt_org'] = config.get('GPT', 'org')
    gOpts['gpt_key'] = config.get('GPT', 'key')
    gOpts['discord_url'] = config.get('DISCORD', 'url')
    gDiscord = Discord(url=gOpts['discord_url'])
    log('done')

def readRules():
    '''
    Read GPT rules file
    '''
    global gOpts
    log('reading rules')
    gOpts['gpt_base'] = listFromFile('rules.txt')
    if len (gOpts['gpt_base']) >= 1:
        log('done')
    else:
        log('something wrong with rules')
        exit(1)

def selROI(purpose='chatline'):
    '''
    Select ROI and write to config
    '''
    log(f'select {purpose} area')
    # Create a transparent, fullscreen window
    root = tk.Tk()
    root.attributes('-alpha', 0.3)
    root.attributes('-fullscreen', True)
    # Set the window on top
    root.attributes('-topmost', True)
    canvas = tk.Canvas(root, bg='white')
    canvas.pack(fill=tk.BOTH, expand=True)

    # Bind the events
    start_x, start_y = None, None
    def on_mouse_down(event):
        nonlocal start_x, start_y
        start_x, start_y = event.x, event.y
    canvas.bind('<Button-1>', on_mouse_down)

    def on_mouse_move(event):
        nonlocal start_x, start_y
        canvas.delete('selection_rectangle')
        canvas.create_rectangle(start_x, start_y, event.x, event.y, 
                                outline='red', tags='selection_rectangle')
    canvas.bind('<B1-Motion>', on_mouse_move)

    def on_mouse_up(event):
        nonlocal start_x, start_y
        global gOpts
        root.destroy()
        end_x, end_y = event.x, event.y
        x, y, w, h = start_x, start_y, end_x - start_x, end_y - start_y
        if w < 0:
            x, w = x + w, -w
        if h < 0:
            y, h = y + h, -h

        area=(x,y,w,h)
        log(area)
        log(f'{purpose} area selected')



        config = ConfigObj('config.ini', encoding='utf8', write_empty_values=True)
        config['XYWH'][purpose] = area
        config.write()

        gOpts[purpose] = area
        
        #print(f'Selected area: ({x}, {y}, {w}, {h})')

    canvas.bind('<ButtonRelease-1>', on_mouse_up)

    # Run the window
    root.mainloop()
    root.quit()


def askGPT(msg):
    '''
    GPT interaction
    '''
    log('gpt client started')
    os.environ['OPENAI_API_KEY'] = gOpts['gpt_key']
    client = OpenAI(
    organization=gOpts['gpt_org'],
    timeout=3
    )

    prompt = f'{gOpts["gpt_base"]}. The question is: {msg} ?'

    chat_completion = client.chat.completions.create(
        messages=[
            {
                "role": "user",
                "content": prompt,
            }
        ],
        model="gpt-3.5-turbo",
    )

    result = chat_completion.choices[0].message.content
    if len(result)> 1:
        log(f'gpt answer: {result}')
        return result
    else:
        return None



def colorSegGreen(frame):
    '''
    Color segmentation (green)
    '''
    frameHSV = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
    eL = np.array([40,137,123])
    eH = np.array([80,257,283])
    mask = cv2.inRange(frameHSV,eL,eH)
    if cv2.countNonZero(mask) > 100:
        cv2.imwrite('ocr.png',mask)
        return True
    else:
        return False

def listFromFile(filename):
  '''
  Create list from txt file
  '''
  with open(filename, 'r') as f:
    lines = f.readlines()

  lines = [line.rstrip() for line in lines]
  return lines

def anProc(frame):
    '''
    Main processing logic
    '''
    
    # Color segmentation. No color = nothing to process
    if not colorSegGreen(frame):
        return False
    
    # OCR. No results - ...
    if not ocrLocal():
        return False
    
    # Text was recognized successfully, let's send it to GPT
    if len(gMsg) == 1:
        index = 0
    elif len(gMsg) > 1:
        index = len(gMsg)-1
    gptResponse = askGPT(f'sender: {gMsg[index][1]} message: {gMsg[index][2]}')

    match = re.search('discord:allowed,msg: (.+)',gptResponse)
    if match:
            sender = gMsg[index][1]  # The second word
            text = match.group(1)  # The remaining part of the string
            msg = f'sender:{sender} text={text}'
            gDiscord.post(content=msg)


    

    

    
def showHelp():
    '''
    Displays mini-help in console
    '''
    print (
    '''
    Mortal Online 2 guildchat to discord gateway.
    

    Hotkeys:
    F1 - start gateway
    F2 - draw a border around chatline
    F3 - stop gateway (press and hold for 1 sec)
    F4 - reload gateway (press and hold for 1 sec)

    Copyright 2024 pmb
    '''
    )


def setHK():
    '''
    Setting hotkeys (except reload/stop)
    '''
    log('setting hotkeys')
    keyboard.add_hotkey('F1', startGw)
    keyboard.add_hotkey('F2', selROI)
    #keyboard.add_hotkey('F3', stop_bot)
    #keyboard.add_hotkey('F4', reload_bot)
    log('done')

def watchdog():
    '''
    Separated thread. This is to guarantee start/stop button availability in any case
    '''
    while True:
        if keyboard.is_pressed('F3'):
            stop_bot()
        if keyboard.is_pressed('F4'):
            reload_bot()
        time.sleep(0.2)

def startGw():
    '''
    Gateway start (after F1)
    '''
    startBG(bgCam)
    startBG(bgAntiAFK)
    #startBG(setHK) #once more time to get the right prio

# Here we go....
readConfig()
readRules()
setHK()
startBG(watchdog)
log('ready')
showHelp()
input()
